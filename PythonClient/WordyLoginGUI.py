import Wordy
from WordyPackage import WrongPasswordOrUsernameException, AccountDoesNotExistException, AccountIsLoggedInException
from tkinter import *

class WordyLoginGUI:
    def __init__(self, wordy_imp):
        self.login_panel = Frame()
        self.password_field = Entry(self.login_panel, show="*")
        self.enter_button = Button(self.login_panel, text="Enter", command=self.login)
        self.username_field = Entry(self.login_panel)

        self.set_title("Wordy Client")
        self.set_default_close_operation(EXIT_ON_CLOSE)
        self.set_content_pane(self.login_panel)
        self.set_location_relative_to(None)
        self.enter_button.pack()

        self.enter_button.bind("<Button-1>", self.login)

        self.pack()
        self.mainloop()

    def login(self):
        username = self.username_field.get()
        password = self.password_field.get()

        if not username or not password:
            messagebox.showerror("Error", "Username and Password must not be empty!")
        else:
            try:
                wordy_imp.login(username, password)
                new_window = WordyMenuGUI(wordy_imp, username)
                new_window.mainloop()
                messagebox.showinfo("Success", "Successfully logged in!")
            except WrongPasswordOrUsernameException as wpoue:
                messagebox.showerror("Error", wpoue.msg)
            except AccountDoesNotExistException as adnee:
                messagebox.showerror("Error", adnee.msg)
            except AccountIsLoggedInException as ailie:
                messagebox.showerror("Error", ailie.msg)

