import WordyApp.Wordy
from WordyApp.WordyPackage import CriteriaNotMetException, InvalidWordException, WordTooShortException
from sun.plugin2.message import JavaObjectOpMessage

from javax.swing import JFrame, JLabel, JTextField, JButton, JPanel
from java.util import Timer, TimerTask
from java.util.concurrent import ExecutorService, Executors, Future

class WordyPlayGUI(JFrame):
    def __init__(self, wordy, username, lobbyNum, wins):
        super().__init__()
        self.setTitle("Current user: " + username)
        self.setDefaultCloseOperation(EXIT_ON_CLOSE)
        self.setContentPane(self.playPanel)
        self.setLocationRelativeTo(None)

        self.letters.setText(wordy.get_letters(lobbyNum))
        self.isHost = wordy.is_host(username, lobbyNum)
        self.wins = wins
        wordy.update_round(1, lobbyNum)

        self.submitWordButton.addActionListener(self.submitWordButton_action)
        self.quitButton.addActionListener(self.quitButton_action)

        self.timerExec = Executors.newSingleThreadExecutor()
        self.timerFuture = self.timerExec.submit(self.timer)

        self.checkExec = Executors.newSingleThreadExecutor()
        self.checkFuture = self.checkExec.submit(self.check)

        self.pack()
        self.setVisible(True)

    def submitWordButton_action(self, event):
        self.enteredString = self.inputWord.getText()
        self.updateCondition()

    def quitButton_action(self, event):
        if self.isHost:
            wordy.remove_lobby(lobbyNum)
        else:
            wordy.remove_player_on_quit(lobbyNum)

        self.timer.cancel()
        self.stopCheckThread()
        self.setVisible(False)
        new WordyMenuGUI(wordy, username)

    def check(self):
        previousString = ""
        while not self.stopThread:
            try:
                self.lock.wait()
                if self.enteredString != previousString:
                    wordy.verify_guess(self.enteredString, username, lobbyID)
                    previousString = self.enteredString
                else:
                    self.response.setText("You just submitted this word.")

            except InvalidWordException as iwe:
                self.response.setText(iwe.msg)
            except WordTooShortException as wtse:
                self.response.setText(wtse.msg)
            except InterruptedException as ex:
                ex.printStackTrace()

    def updateCondition(self):
        self.conditionChanged = True
        self.lock.notifyAll()

    def stopCheckThread(self):
        self.stopThread = True

    def timer(self):
        timer = Timer()

        timerTask = TimerTask()

        timerTask.run = lambda: self.timerCntr.setText(str(counter)) if counter > 0 else (
            if wins != 3:
                wins += wordy.get_score(username, lobbyID)
                scoredWins.setText(str(wins))

                if self.isHost:
                    wordy.reassign_letters(lobbyID, username)
                    wordy.update_round(0, lobbyID)
                    self.setVisible(False)
                    new WordyPlayGUI(wordy, username, lobbyID, wins)
                else:
                    self.setVisible(False)
                    new WordyPlayGUI(wordy, username, lobbyID, wins)
            else:
                self.setVisible(False)
                stopCheckThread()
                JOptionPane.showMessageDialog(None, "Game Over.")
                new WordyMenuGUI(wordy, username)
        )

        timer.schedule(timerTask, 0, 1000)

