import javax.swing as swing

class WordyLeaderboardGUI(swing.JFrame):
    def __init__(self, wordy, username):
        super().__init__()
        self.setTitle("Highest Win Ranking")
        self.setDefaultCloseOperation(swing.JFrame.EXIT_ON_CLOSE)
        self.lengthiestPanel = swing.JPanel()
        self.lead = swing.JLabel("Highest Win Ranking")
        self.backButton = swing.JButton("Back")
        self.table = swing.JTable()
        self.leadDesc = swing.JLabel("Players | Number of Wins")
        self.scPane = swing.JScrollPane()
        self.add(self.lengthiestPanel)
        self.pack()
        self.setVisible(True)

        try:
            ldbList = wordy.leaderboards()
            ldbArr = ldbList.split(",")
            model = swing.table.DefaultTableModel()
            model.addColumn("Players")
            model.addColumn("Number of Wins")
            for i in range(0, len(ldbArr), 2):
                user = ldbArr[i]
                wins = int(ldbArr[i + 1])
                rowData = [user, wins]
                model.addRow(rowData)
            self.table.setModel(model)
            self.scPane.setViewportView(self.table)
        except EmptyListException as e:
            swing.JOptionPane.showMessageDialog(None, e.getMessage())

        self.backButton.addActionListener(self.back_button_clicked)

    def back_button_clicked(self, event):
        new WordyMenuGUI(wordy, username)
        self.setVisible(False)

if __name__ == "__main__":
    wordy = Wordy()
    username = "player1"
    new WordyLeaderboardGUI(wordy, username)
