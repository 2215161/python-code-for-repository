import Wordy
from WordyPackage import CriteriaNotMetException
from threading import Timer
from concurrent.futures import ThreadPoolExecutor

class WordyLobbyGUI:

    def __init__(self, wordy, username):
        self.lobbyNum = wordy.create_or_join_lobby(username)
        self.isHost = wordy.is_host(username, self.lobbyNum)
        if not self.isHost:
            self.timer_checker(wordy, username)

        self.cancelButton.clicked.connect(lambda: self.close(wordy, username))

        self.timerExec = ThreadPoolExecutor()
        self.timerFuture = self.timerExec.submit(self.timer, wordy, username)
        if self.isHost:
            wordy.update_round(1, self.lobbyNum)

        self.show()

    def timer(self, wordy, username):
        timer = Timer(1, self.countdown, (wordy, username))
        timer.start()

    def countdown(self, wordy, username):
        counter = 10
        while counter > 0:
            if self.isHost:
                self.txtCnt.setText(str(counter))
            else:
                self.txtCnt.setText("")
            counter -= 1

        try:
            wordy.update_round(0, self.lobbyNum)
            wordy.can_start(self.lobbyNum)
            self.close(wordy, username)
            new WordyPlayGUI(wordy, username, self.lobbyNum, 0)

        except CriteriaNotMetException as e:
            JOptionPane.showMessageDialog(None, e.msg)
            new WordyMenuGUI(wordy, username)
            self.close(wordy, username)

    def timer_checker(self, wordy, username):
        timerChecker = Timer(100, self.check_round_stat, (wordy, username))
        timerChecker.start()

    def check_round_stat(self, wordy, username):
        flag = wordy.get_round_stat(self.lobbyNum)
        if not flag:
            try:
                wordy.can_start(self.lobbyNum)
                self.close(wordy, username)
                new WordyPlayGUI(wordy, username, self.lobbyNum, 0)

            except CriteriaNotMetException as e:
                JOptionPane.showMessageDialog(None, e.msg)
                new WordyMenuGUI(wordy, username)
                self.close(wordy, username)

    def close(self, wordy, username):
        self.timer.cancel()
        self.timerFuture.cancel()
        self.timerExec.shutdown()
        self.dispose()
