class TimerThread(threading.Thread):

    def __init__(self, start=10):
        super().__init__()
        self.start = start
        self.currTime = 0

    def run(self):
        try:
            for i in range(self.start, 0, -1):
                time.sleep(1000)
                self.currTime = i
        except InterruptedError as e:
            raise RuntimeError(e)

    def getTime(self):
        return self.currTime


if __name__ == "__main__":
    timer = TimerThread()
    timer.start()
    print(timer.getTime())