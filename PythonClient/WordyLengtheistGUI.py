import PySimpleGUI as sg
from WordyApp.Wordy import Wordy, EmptyListException

class WordyLengthiestGUI(sg.Window):
    def __init__(self, wordy, username):
        layout = [
            [sg.Table(values=[], headings=['Players', 'Word'], key='-TABLE-', enable_events=True,
                      auto_size_columns=True)],
            [sg.Button('Back')]
        ]
        super().__init__('Highest Win Ranking', layout)
        self.wordy = wordy
        self.username = username

    def run(self):
        try:
            ldb_list = self.wordy.lengthiest()
            ldb_arr = ldb_list.split(",")

            data = [[ldb_arr[i], ldb_arr[i + 1]] for i in range(0, len(ldb_arr), 2)]
            self['-TABLE-'].update(values=data)

        except EmptyListException as e:
            sg.popup(e.getMessage())

        while True:
            event, _ = self.read()
            if event == sg.WINDOW_CLOSED:
                break
            elif event == 'Back':
                WordyMenuGUI(self.wordy, self.username).run()
                break

        self.close()


class WordyMenuGUI:
    def __init__(self, wordy, username):
        self.wordy = wordy
        self.username = username

    def run(self):
        # Add your code for WordyMenuGUI here
        pass


if __name__ == '__main__':
    wordy = Wordy()  # Create an instance of the Wordy class
    username = ""  # Set the username

    WordyLengthiestGUI(wordy, username).run()
