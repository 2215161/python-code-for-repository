import wordy
from wordy.exceptions import CriteriaNotMetException, EmptyListException
from wordy.lengthiest_map import LengthiestMap

from javax.swing import JFrame, JLabel, JButton, JPanel


class WordyMenuGUI(JFrame):

    def __init__(self, wordy, username):
        super().__init__()
        self.setTitle("Current user: {}".format(username))
        self.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
        self.setContentPane(self.menuPanel)
        self.setLocationRelativeTo(None)

        self.greeting.setText("Welcome {}".format(username))

        self.playButton.addActionListener(lambda e: self.play())
        self.leaderboardButton.addActionListener(lambda e: self.leaderboard())
        self.wordRankingButton.addActionListener(lambda e: self.word_ranking())
        self.logoutButton.addActionListener(lambda e: self.logout())

        self.pack()
        self.setVisible(True)

    def play(self):
        self.setVisible(False)
        new_window = WordyLobbyGUI(wordy, username)
        new_window.setVisible(True)

    def leaderboard(self):
        self.setVisible(False)
        new_window = WordyLeaderboardGUI(wordy, username)
        new_window.setVisible(True)

    def word_ranking(self):
        try:
            longest_words = LengthiestMap.get_longest_words(wordy)
        except CriteriaNotMetException:
            print("No words found that meet the criteria.")
        except EmptyListException:
            print("No words found.")
        else:
            self.setVisible(False)
            new_window = WordyLengthiestGUI(wordy, username, longest_words)
            new_window.setVisible(True)

    def logout(self):
        wordy.logout(username)
        self.setVisible(False)
        System.exit(0)

