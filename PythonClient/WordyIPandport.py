import javax.swing as swing

class WordyIpAndPort(swing.JFrame):
    def __init__(self):
        super().__init__()
        self.setTitle("Setting up I P address and port")
        self.setDefaultCloseOperation(swing.JFrame.EXIT_ON_CLOSE)
        self.mainPanel = swing.JPanel()
        self.txtIP = swing.JTextField()
        self.txtPort = swing.JTextField()
        self.joinBtn = swing.JButton("Join")
        self.joinBtn.addActionListener(self.join_button_clicked)
        self.add(self.mainPanel)
        self.pack()
        self.setVisible(True)

    def join_button_clicked(self, event):
        try:
            props = Properties()
            props.put("org.omg.CORBA.ORBInitialHost", self.txtIP.getText())
            props.put("org.omg.CORBA.ORBInitialPort", self.txtPort.getText())
            orb = ORB.init((str,)[])
            objRef = orb.resolve_initial_references("NameService")
            ncRef = NamingContextExtHelper.narrow(objRef)
            name = "Wordy"
            wordyImp = WordyHelper.narrow(ncRef.resolve_str(name))
            JOptionPane.showMessageDialog(None, "Successfully Connected.")
            new WordyLoginGUI(wordyImp)
        except Exception as ex:
            JOptionPane.showMessageDialog(None, "Something went wrong. Try again later.")
            new WordyIpAndPort()

if __name__ == "__main__":
    new WordyIpAndPort()
